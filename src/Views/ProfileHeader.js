import {View} from 'backbone';
import $ from "jquery";

const ProfileHeader = View.extend({
    initialize: function() {
        App.events.on("RenderProfile", this.render);
    },

    events: {
        //on click perform clickhandler
        'click' : 'clickHandler'
    },

    /**
     * gets the entered id and gets the data
     * @param e
     */
    clickHandler: function (e) {
        e.preventDefault();

        $('#id-entry').hide();

        //get steam id from input
        let steamUser = document.getElementById("userID").value;
        //when clicked it triggers GetProfile
        App.events.trigger("GetProfile", steamUser);

    },

    /**
     * renders entered steamuser
     * @param data
     * @returns {ProfileHeader}
     */
    render: function (data) {
        var template = Handlebars.compile($('#profile-template').html());
        $('#profile').html(template(data.players[0]));
        return this;
    }

});


export default ProfileHeader;