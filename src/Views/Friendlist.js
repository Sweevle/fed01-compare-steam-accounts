import {View} from 'backbone';
import $ from "jquery";


const Friendlist = View.extend({
    initialize: function() {
        App.events.on("RenderFriends", this.render);
    },

    events: {
        //on click perform clickhandler
        'click' : 'clickHandler'
    },

    clickHandler: function (e) {
        e.preventDefault();

        //get steam id from input
        let steamUser = document.getElementById("userID").value;
        //when clicked it triggers GetProfile
        App.events.trigger("GetFriends", steamUser);

    },

    /**
     * renders friendslist
     * @param data
     * @returns {Friendlist}
     */
    render: function (data) {

        let template = Handlebars.compile($('#friends-template').html());
        $('#friendlist').html(template(data));
        return this;
    }

});

export default Friendlist;