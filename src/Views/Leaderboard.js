import {View} from 'backbone';
import $ from "jquery";

const Leaderboard = View.extend({
    initialize: function() {
        App.events.on("RenderLeaderboard", this.render);
    },

    events: {
        //on click perform clickhandler
        'click' : 'clickHandler'
    },

    clickHandler: function (e) {
        e.preventDefault();

        //get steam id from input
        let steamUser = document.getElementById("userID").value;
        //when clicked it triggers friendGames
        App.events.trigger("friendGames", steamUser);

    },

    /**
     * renders leaderboard
     * @param data
     * @returns {Leaderboard}
     */
    render: function (data) {
        let leaderboard = data;

        let template = Handlebars.compile($('#hours-played-template').html());
        $('#leaderboard').html(template(leaderboard));
        return this;
    }
});

export default Leaderboard;