import SteamCalculator from './SteamCalculator';


const LeaderboardModel = SteamCalculator.extend({
    initialize: function () {
        App.events.on("friendGames", this.getFriends, this);
        App.events.on("getNames", this.getUserName, this);
        App.events.on("calculate", this.calculateTimePlayed, this);
    },

    model: SteamCalculator,

    /**
     * Gets the friendlist of the user
     * @param steamid
     */
    getFriends: function (steamid) {
        LeaderboardModel.__super__.getFriends(steamid).then(this.getIndividualFriendGames);
    },

    /**
     * Parses the friendlist into steamids and gets the owned games
     * @param data
     */
    getIndividualFriendGames: function (data) {
        let friendslist = data.friendslist.friends;

        /**
         * shuffels the friend id's
         * @param array
         * @returns {*}
         */
        var shuffleArray = (array) =>
        {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        };

        var results = shuffleArray(friendslist);

        let steamids = results.map(function(obj){
            return obj.steamid;
        });

        steamids.push($('#userID').val());

        /**
         * Excecutes the promises and parses the result to individual user objects
         * Alco checks if the user is not private or has no games
         * @param promises
         */
        let doPromise = (promises) => {
            Promise.all(promises).then(function (data) {

                let nonPrivates = data.map(function(obj, i) {

                    let hasGames = obj.hasOwnProperty('game_count');
                    if ( hasGames == true || obj.game_count > 0) {
                        let userData = [];
                        userData["games_count"] = obj.game_count;
                        userData["games"] = obj.games;
                        userData["steamid"] = steamids[i];
                        return userData;
                    }

                }).filter(function(item){ return item !== undefined;});

                App.events.trigger("getNames", nonPrivates);

            }, function (err) {
                // error occurred
                console.log(err);
            });
        };


        //puts all the promises in array and then excecute all the promises
        //if user has more than 100 friends if will take 100 friends
        if (steamids.length > 100){
                let steamidsOnehundred = steamids.slice(0, 99);

                let getGamesPromises = steamidsOnehundred.map(function(obj){
                    return LeaderboardModel.__super__.getOwnedGames(obj);
                });

                doPromise(getGamesPromises);

            } else {
                let getGamesPromises = steamids.map(function(obj){
                    return LeaderboardModel.__super__.getOwnedGames(obj);
                });

                doPromise(getGamesPromises);
            }



    },

    /**
     * gets the username that belongs to the user
     * @param data
     */
    getUserName: function (data) {

        let getNamePromises = data.map(function(obj){
            return LeaderboardModel.__super__.getName(obj.steamid);
        });

        Promise.all(getNamePromises).then(function(result) {


            var completeArray = result.map(function(obj, i) {
                    let userData = [];
                    userData["games"] = data[i].games;
                    userData["games_count"] = data[i].games_count;
                    userData["steamname"] = obj.personaname;
                    userData["profileurl"] = obj.profileurl;
                    return userData;
            });


            App.events.trigger("calculate", completeArray);

        }, function(err) {
            // error occurred
            console.log(err);
        });

    },

    /**
     * calculates the total time played, sum of all games
     * @param data
     */
    calculateTimePlayed: function (data) {

            let timePlayedPerUser = data.map(function(obj, i) {
                let totalTime = {};
                var minutes = 0;
                let amountOfGames = obj.games.length;

                for (var j = 0; j < amountOfGames; j++){
                    minutes += data[i].games[j].playtime_forever;
                }


                let hours = Math.floor( minutes / 60);
                let realMinutes = minutes % 60;

                totalTime["user"] = data[i].steamname;
                totalTime["profileurl"] = data[i].profileurl;
                totalTime["hours"] = hours;
                totalTime["minutes"] = minutes;
                totalTime["playtime"] = hours + " hours " + realMinutes + " minutes";

                return totalTime;

            });

            let playtime = timePlayedPerUser.map(function(obj){
                return obj.minutes;
            });

            let highestValueIndex =  playtime.indexOf(Math.max.apply(null,playtime));

            let nolifer = [];
            let userIndex = timePlayedPerUser.length - 1;

            nolifer["top"] = timePlayedPerUser[highestValueIndex];
            nolifer["ownPlaytime"] = timePlayedPerUser[userIndex].playtime;

            App.events.trigger("RenderLeaderboard", nolifer);
    }



});

export default LeaderboardModel;



