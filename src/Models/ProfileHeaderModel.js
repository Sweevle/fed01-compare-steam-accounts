import SteamCalculator from './SteamCalculator';

const ProfileHeaderModel = SteamCalculator.extend({
    initialize: function () {

        App.events.on("GetProfile", this.getUser, this);

    },

    model: SteamCalculator,

    /**
     * gets the data of the entered steam user
     * @param steamid
     */
    getUser: function (steamid) {
        ProfileHeaderModel.__super__.getProfile(steamid).then(this.passUserInfo);
    },

    /**
     * returns the steam users data to the view
     * @param data
     */
    passUserInfo: function (data) {
        App.events.trigger("RenderProfile", data.response);
    },
});

export default ProfileHeaderModel;



