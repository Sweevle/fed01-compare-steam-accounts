import SteamCalculator from './SteamCalculator';

const FriendlistModel = SteamCalculator.extend({
    initialize: function () {
        App.events.on("GetFriends", this.getFriends, this);
    },

    model: SteamCalculator,

    /**
     * Gets the friendlist of the user
     * @param steamid
     */
    getFriends: function (steamid) {
        FriendlistModel.__super__.getFriends(steamid).then(this.getIndividualFriendInfo);
    },

    /**
     * Parses the friendlist into steamids and gets the owned games
     * @param data
     */
    getIndividualFriendInfo: function (data) {
        let friendslist = data.friendslist.friends;

        /**
         * shuffels the friend id's
         * @param array
         * @returns {*}
         */
        var shuffleArray = (array) =>
        {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        };

        var results = shuffleArray(friendslist);

        var steamids = [];
        for (var i = 0; i < 14; i++ ){
            steamids.push(results[i].steamid);
        }

        /**
         * triggers renderfriends event
         * @param friends
         */
        var getFriendInfo = (friends) =>
        {
            App.events.trigger("RenderFriends", friends.response.players);
        };

        FriendlistModel.__super__.getProfile(steamids).then(getFriendInfo);

    }

});

export default FriendlistModel;



