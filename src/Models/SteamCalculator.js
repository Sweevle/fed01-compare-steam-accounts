import {Model} from 'backbone';
import reqwest from 'reqwest';

const SteamCalculator = Model.extend({
    defaults: {
        "steamkey": "FC55D687B11C01E79CD811E41BDDF918",
    },

    /**
     * gets userprofile
     * @param id
     * @param data
     * @returns {Promise}
     */
    getProfile: function (id, data) {

        return new Promise((resolve, reject) => {
        //Default ajax parameters
        let parameters = {
            type: 'json',
            url: "http://nodejs.codeplex.nl/steamapi/ISteamUser/GetPlayerSummaries/v0002/?key="+ this.defaults.steamkey +"&steamids=" + id,
            success: (result) => resolve(result),
            error: (err) => reject(err)
        };

            //If data is passed, add it to the AJAX parameters
            if (data) {
                parameters.data = data;
            }

            //Actual AJAX call
            reqwest(parameters);
        });
    },

    /**
     * gets friendlist
     * @param id
     * @param data
     * @returns {Promise}
     */
    getFriends: function (id, data)
    {
        return new Promise((resolve, reject) => {
        let parameters = {
            type: 'json',
            url: "http://nodejs.codeplex.nl/steamapi/ISteamUser/GetFriendList/v0001/?key="+ this.defaults.steamkey +"&steamid=" + id + "&relationship=friend",
            success: (result) => resolve(result),
            error: (err) => reject(err)
        };

            //If data is passed, add it to the AJAX parameters
            if (data) {
                parameters.data = data;
            }

            //Actual AJAX call
            reqwest(parameters);
        });
    },

    /**
     * gets owned games
     * @param id
     * @param data
     * @returns {Promise}
     */
    getOwnedGames: function (id, data)
    {
        return new Promise((resolve, reject) => {

            //Default ajax parameters
            let parameters = {
                type: 'json',
                url: "http://nodejs.codeplex.nl/steamapi/IPlayerService/GetOwnedGames/v0001/?key=" + this.defaults.steamkey + "&steamid=" + id + "&format=json&include_appinfo=1&include_played_free_games=1",
                success: (result) => resolve(result.response),
                error: (err) => reject(err)
            };

            //If data is passed, add it to the AJAX parameters
            if (data) {
                parameters.data = data;
            }

            //Actual AJAX call
            reqwest(parameters);
        });
    },

    /**
     * gets just the username
     * @param id
     * @param data
     * @returns {Promise}
     */
    getName: function (id, data) {

        return new Promise((resolve, reject) => {
            //Default ajax parameters
            let parameters = {
                type: 'json',
                url: "http://nodejs.codeplex.nl/steamapi/ISteamUser/GetPlayerSummaries/v0002/?key=" + this.defaults.steamkey + "&steamids=" + id,
                success: (steamname) => resolve(steamname.response.players[0]),
                error: (err) => reject(err)
            };

            //If data is passed, add it to the AJAX parameters
            if (data) {
                parameters.data = data;
            }

            //Actual AJAX call
            reqwest(parameters);
        });
    }

});



export default SteamCalculator;



