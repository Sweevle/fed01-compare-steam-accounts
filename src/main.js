import _ from 'underscore';
import {Events} from 'backbone';
import ProfileHeaderModel from './Models/ProfileHeaderModel';
import ProfileHeader from './Views/ProfileHeader';
import FriendlistModel from './Models/FriendlistModel';
import Friendlist from './Views/Friendlist';
import Leaderboard from './Views/Leaderboard';
import LeaderboardModel from './Models/LeaderboardModel'

require('es6-promise').polyfill();

(function ()
{
    let setGlobalVariables = function (){
        window.App = {};
        App.events = _.clone(Events);
    };

    /**
     * Run after dom is ready
     */
    let init = function ()
    {
        setGlobalVariables();
        let enteredUser = new ProfileHeaderModel();
        new ProfileHeader({el: "#submit", model: enteredUser});

        let Friendslist = new FriendlistModel();
        new Friendlist({el: "#submit", model: Friendslist});

        let leaderboard = new LeaderboardModel();
        new Leaderboard({el: "#submit", model: leaderboard});

    };

    window.addEventListener('load', init);
})();